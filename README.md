#DuxAdmin

DuxAdmin是一款基于Canphp框架与CanApp模式开发的一款通用管理后台框架

采用CanApp作为底层框架，经过多方面的优化改进，将各个功能模块APP化，通过各个APP之间的API来进行交互，达到的可分离开发扩展极其方便的效果。

集成后台UI组件，集成菜单与权限API，免去设计与开发烦恼

基于php5.2+、mysql5.1+ 开发的,低于此版本无法使用。

#说明

默认后台帐号密码为：admin
默认后台地址为：http://域名/index.php?r=admin

----

#交流

反馈建议：http://git.oschina.net/duxcms/DuxAdmin/issues

QQ群：131331864
 	
网址：http://www.duxcms.com