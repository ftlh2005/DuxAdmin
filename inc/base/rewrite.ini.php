<?php 
//伪静态规则
$config['REWRITE']['class/<urlname>-page-<page>.html'] = 'content/category/index';
$config['REWRITE']['class/<urlname>.html'] = 'content/category/index';
$config['REWRITE']['page/<urlname>-page-<page>.html'] = 'pages/category/index';
$config['REWRITE']['page/<urlname>.html'] = 'pages/category/index';
$config['REWRITE']['jump/<urlname>-page-<page>.html'] = 'jump/category/index';
$config['REWRITE']['jump/<urlname>.html'] = 'jump/category/index';
$config['REWRITE']['form/<form>-page-<page>.html'] = 'form/index/index';
$config['REWRITE']['form/<form>.html'] = 'form/index/index';
$config['REWRITE']['tags/index-page-<page>.html'] = 'tags/index/index';
$config['REWRITE']['tags/index.html'] = 'tags/index/index';
$config['REWRITE']['tags/<name>-page-<page>.html'] = 'tags/index/info';
$config['REWRITE']['tags/<name>.html'] = 'tags/index/info';
$config['REWRITE']['/<dirs>/<urltitle>-page-<page>.html'] = 'content/info/index';
$config['REWRITE']['/<dirs>/<urltitle>.html'] = 'content/info/index';
