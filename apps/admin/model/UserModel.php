<?php
/**
 * UserModel.php
 * 用户表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class UserModel extends BaseModel
{
    protected $table = 'user';
    /**
     * 获取用户列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 列表
     */
    public function loadData($condition = null, $limit = 20)
    {
        return $this->model->field('A.*,B.name AS group_name')->table('user', 'A')->leftJoin('user_group', 'B', array(
            'A.group_id',
            'B.group_id'
        ))->where($condition)->order('user_id DESC')->limit($limit)->select();
    }
    /**
     * 获取用户总数
     * @param string $condition 条件
     * @return int 条数
     */
    public function countData($condition = null)
    {
        return $this->model->table('user', 'A')->leftJoin('user_group', 'B', array(
            'A.group_id',
            'B.group_id'
        ))->where($condition)->order('user_id DESC')->count();
    }
    /**
     * 用户信息
     * @param int $userId 用户ID
     * @return array 用户信息
     */
    public function getUser($userId)
    {
        return $this->find('user_id=' . $userId);
    }
    /**
     * 查找用户信息
     * @param string $userName 用户名
     * @return array 用户信息
     */
    public function getUserInfo($userName, $userId = null)
    {
        if ($userId) {
            return $this->find(('username="' . $userName) . '" AND user_id<>' . $userId);
        } else {
            return $this->find(('username="' . $userName) . '"');
        }
    }
    /**
     * 查找用户信息
     * @param string $email 邮箱
     * @param string $userId 排除用户ID
     * @return array 用户信息
     */
    public function getUserEmail($email, $userId = null)
    {
        if ($userId) {
            return $this->find(('email="' . $email) . '" AND user_id<>' . $userId);
        } else {
            return $this->find(('email="' . $email) . '"');
        }
    }
    /**
     * 添加用户信息
     * @param array $data 用户信息
     * @return int 用户ID
     */
    public function addData($data)
    {
        return $this->insert($data);
    }
    /**
     * 保存用户信息
     * @param array $data 用户信息
     * @return bool 状态
     */
    public function saveData($data)
    {
        return $this->update('user_id=' . $data['user_id'], $data);
    }
    /**
     * 删除用户信息
     * @param int $userId 用户ID
     * @return bool 状态
     */
    public function delData($userId)
    {
        return $this->delete('user_id=' . $userId);
    }
    /**
     * 根据用户组删除
     * @param int $groupId 用户组ID
     * @return bool 状态
     */
    public function delGroupData($groupId)
    {
        return $this->delete('group_id=' . $groupId);
    }
}