<?php
/**
 * MenuModel.php
 * 附件数据管理
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class AttachmentModel extends BaseModel
{
    protected $table = 'attachment';
    /**
     * 根据ID获取附件列表
     * @param string $fileId 附件ID
     * @return array 附件列表
     */
    public function getFileList($fileId)
    {
        return $this->select(('file_id in(' . $fileId) . ')', '', 'file_id asc');
    }
    /**
     * 保存附件信息
     * @param array $data 附件信息
     * @return array 附件完整信息
     * @return array 附件信息
     */
    public function addData($data)
    {
        $id = $this->insert($data);
        $data['file_id'] = $id;
        return $data;
    }
    /**
     * 关联附件信息
     * @param array $data 附件信息
     * @return array 附件完整信息
     */
    public function relationData($relationId,$model,$relationKey)
    {
        $data=array();
        $data['model']=$model;
        $data['relation_id']=$relationId;
        return $this->update('relation_key='.$relationKey,$data);
    }
    /**
     * 删除关联附件信息
     * @param array $data 附件信息
     * @return array 附件完整信息
     */
    public function delRelationData($relationId,$model)
    {
        $data=array();
        $data['model']=$model;
        $data['relation_id']=$relationId;
        $list = $this->select($data);
        if(!empty($list)){
            foreach ($list as $value) {
                @unlink(ROOT_PATH.$value['url']);
                @unlink(ROOT_PATH.$value['original']);
            }
        }
        //删除API
        hook_api('ApiAttachmentDelRelation',$list);
        return $this->delete('relation_id in('.$relationId.') AND model="'.$model.'"');
    }
    
}