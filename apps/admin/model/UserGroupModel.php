<?php
/**
 * UserGroupModel.php
 * 用户组表操作
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class UserGroupModel extends BaseModel
{
    protected $table = 'user_group';
    /**
     * 获取用户组列表
     * @param string $condition 条件
     * @param int $limit 数量
     * @return array 用户组列表
     */
    public function loadData($condition = null, $limit = 20)
    {
        return $this->select($condition, '', 'group_id ASC', $limit);
    }
    /**
     * 获取用户组总数
     * @param string $condition 条件
     * @return int 数量
     */
    public function countData($condition = null)
    {
        return $this->count($condition);
    }
    /**
     * 用户组信息
     * @param int $groupId 用户组ID
     * @return array 用户信息
     */
    public function getInfo($groupId)
    {
        return $this->find('group_id=' . $groupId);
    }
    /**
     * 添加用户组信息
     * @param array $data 用户组信息
     * @return int 用户组ID
     */
    public function addData($data)
    {
        return $this->insert($data);
    }
    /**
     * 保存用户组信息
     * @param array $data 用户组信息
     * @return bool 状态
     */
    public function saveData($data)
    {
        return $this->update('group_id=' . $data['group_id'], $data);
    }
    /**
     * 删除用户组信息
     * @param int $groupId 用户组ID
     * @return bool 状态
     */
    public function delData($groupId)
    {
        $userList = model('User')->delGroupData($groupId);
        return $this->delete('group_id=' . $groupId);
    }
    /**
     * 获取功能权限
     * @return array 状态
     */
    public function getAdminPurview()
    {
        return hook_api('apiAdminPurview');
    }
}