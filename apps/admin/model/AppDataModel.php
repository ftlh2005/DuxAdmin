<?php
/**
 * AppDataModel.php
 * 系统功能管理
 * @author Life <349865361@qq.com>
 * @version 20140111
 */
class AppDataModel extends BaseModel
{
    /**
     * 获取APP列表
     */
    public function loadList()
    {
        $list = getApps();
        if (!empty($list)) {
            foreach ($list as $value) {
                $data[$value] = appConfig($value);
            }
        }
        return $data;
    }
    /**
     * 安装数据库
     * @param string $dbDir 数据库目录
     * @param array $dbConfig 数据库配置文件
     * @param string $dbPrefix 需要替换表前缀
     */
    public function installData($dbDir, $dbConfig, $dbPrefix)
    {
        $db = @new Dbbak($dbConfig['DB_HOST'] . ':' . $dbConfig['DB_PORT'], $dbConfig['DB_USER'], $dbConfig['DB_PWD'], $dbConfig['DB_NAME'], 'utf8', $dbDir);
        return @$db->importSql('', $dbPrefix, $dbConfig['DB_PREFIX']);
    }
    /**
     * 导出数据库
     * @param string $dbDir 数据库目录
     * @param array $dbConfig 数据库配置文件
     * @param string $table 数据表,分割多个
     */
    public function backupData($dbDir, $dbConfig, $table)
    {
        $db = new Dbbak($dbConfig['DB_HOST'], $dbConfig['DB_USER'], $dbConfig['DB_PWD'], $dbConfig['DB_NAME'], 'utf8', $dbDir);
        $tables = explode(',', $table);
        foreach ($tables as $table) {
            $table_array[] = $dbConfig['DB_PREFIX'] . $table;
        }
        return $db->exportSql($table_array);
    }
    /**
     * 卸载数据库
     * @param string $table 数据表,分割多个
     */
    public function uninstallData($table)
    {
        if (empty($table)) {
            return;
        }
        $tables = explode(',', $table);
        $dbConfig = config('DB');
        foreach ($tables as $table) {
            $table = '`' . $dbConfig['DB_PREFIX'] . trim($table) . '`';
            $this->query("DROP TABLE $table ");
        }
        return true;
    }
}