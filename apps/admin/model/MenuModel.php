<?php
/**
 * MenuModel.php
 * 菜单数据管理
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class MenuModel extends BaseModel
{
    /**
     * 获取指定菜单列表
     * @param string $key 可选主菜单KEY
     * @return array 菜单数组
     */
    public function getMenuList($key = null)
    {
        $list = $this->getAdminMenu();
        if (!empty($key)) {
            return $list[$key];
        }

        return $list;
    }
    /**
     * 获取菜单数组
     * @param bool $cache 重建缓存
     * @return array 菜单数组
     */
    public function getAdminMenu($cache = false)
    {
        $cacheFile = ROOT_PATH . 'cache/app_cache/admin_menu.php';
        $file = @file_get_contents($cacheFile);
        if (empty($file) || $cache) {
            $this->cacheAdminMenu();
            $file = @file_get_contents($cacheFile);
        }
        if (empty($file)) {
            return;
        }
        $list = unserialize($file);
        return array_filter($list);
    }
    /**
     * 缓存菜单数组
     */
    public function cacheAdminMenu()
    {
        $list = hook_api('apiAdminMenu');
        $dir = ROOT_PATH . 'cache/app_cache/';
        $cacheFile = $dir . 'admin_menu.php';
        if (!file_exists($dir)) {
            if (!@mkdir($dir, true)) {
                throw new Exception($dir . ' 缓存目录无法创建，请检查目录权限！', 404);
            }
        }
        $menu = array();
        if(!empty($list)){
            //合并菜单
            foreach ($list as $appKey => $value) {
                $menu=array_merge_recursive($menu,$value);
            }
            //排序菜单
            foreach ((array) $menu as $topKey => $top) {
                if (!empty($menu[$topKey]['menu']) && is_array($menu[$topKey]['menu'])) {
                    $menu[$topKey]['menu']=array_order($top['menu'], 'order', 'asc');
                    foreach ((array) $menu[$topKey]['menu'] as $parentKey => $parent) {
                        if (!empty($parent['menu']) && is_array($parent['menu'])) {
                            $menu[$topKey]['menu'][$parentKey]['menu'] = array_order($parent['menu'], 'order', 'asc');
                        }
                    }
                }
            }
            $menu=array_order($menu, 'order', 'asc');
            $html = serialize($menu);
            if (!@file_put_contents($cacheFile, $html)) {
                throw new Exception('APP缓存文件无法写入，请检查目录权限！', 404);
            }


        }

        $menu = array();
        if (!empty($list)) {
            foreach ((array) $list as $appKey => $app) {
                foreach ((array) $app as $topKey => $top) {
                    $menu[$topKey]['name'] = $top['name'];
                    if (!empty($top['menu']) && is_array($top['menu'])) {
                        foreach ((array) $top['menu'] as $parentKey => $parent) {
                            $menu[$topKey]['menu'][$parentKey]['name'] = $parent['name'];
                            $menu[$topKey]['menu'][$parentKey]['menu'] = array_merge((array) $menu[$topKey][$parentKey]['menu'], (array) $parent['menu']);
                        }
                    }
                }
            }
            foreach ((array) $menu as $topKey => $top) {
                if (!empty($top['menu']) && is_array($top['menu'])) {
                    foreach ((array) $top['menu'] as $parentKey => $parent) {
                        if (!empty($parent['menu']) && is_array($parent['menu'])) {
                            $menu[$topKey]['menu'][$parentKey]['menu'] = array_order($parent['menu'], 'order', 'asc');
                        }
                    }
                }
            }
            
        }
    }
}