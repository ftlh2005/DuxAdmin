<?php
/**
 * CacheModel.php
 * 缓存管理
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class CacheModel extends BaseModel
{
    /**
     * 根据类型获取缓存目录
     * @param int $type 类型
     * @return string 目录地址
     */
    public function getFile($type)
    {
        $rootPath = realpath(ROOT_PATH);
        switch ($type) {
            case 1:
                $file = $rootPath . '\cache\app_cache\app_list.php';
                break;
            case 2:
                $file = $rootPath . '\cache\app_cache\admin_menu.php';
                break;
            case 3:
                $file = $rootPath . '\cache\tpl_cache';
                break;
            case 4:
                $file = $rootPath . '\cache\html_cache';
                break;
            case 5:
                $file = $rootPath . '\cache\db_cache\cachedata.php';
                break;
            case 6:
                $file = $rootPath . '\cache\session_cache\cachedata.php';
                break;
        }
        return $file;
    }
    /**
     * 清除缓存
     * @param int $type 类型
     * @return bool 状态
     */
    public function clearCache($type)
    {
        $file=$this->getFile($type);
        if(!file_exists($file)){
            return false;
        }
        if (is_dir($file)) {
            return @del_dir($file);
        } else if (is_file($file)) {
            return @unlink($file);
        }
        return false;
    }
}