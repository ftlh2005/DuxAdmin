<?php
/**
 * UserController.php
 * 用户管理页面
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class UserController extends AdminController
{
    /**
     * 主框架页面
     */
    public function index()
    {
        //筛选条件
        $filterGroudId = intval($_GET['group_id']);
        $filterKeyword = urldecode($_GET['keyword']);
        $filterWhere = '';
        if (!empty($filterGroudId)) {
            $filterWhere .= ' AND A.group_id=' . $filterGroudId;
        }
        if (!empty($filterKeyword)) {
            $filterWhere .= ' AND A.username LIKE "%' . $filterKeyword . '%" OR A.email LIKE "%' . $filterKeyword . '%"';
        }
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => intval($_GET['page']),
            'group_id' => $filterGroudId,
            'keyword' => $filterKeyword
        );
        $url = url('User/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = 'A.level<=' . intval($this->userInfo['level']) . ' AND B.level<=' . intval($this->userInfo['group_level']) . $filterWhere;
        //用户列表信息
        $list = model('User')->loadData($where, $limit);
        $count = model('User')->countData($where);
        //管理组列表信息
        $whereGroup = 'level<=' . intval($this->userInfo['group_level']);
        $groupList = model('UserGroup')->loadData($whereGroup, 20);
        //获取分页
        $page = $this->pageShow($count);
        //模板赋值
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('groupList', $groupList);
        $this->assign('page', $page);
        $this->show();
    }
    /**
     * 添加用户
     */
    public function add()
    {
        //管理组列表信息
        $whereGroup = 'level<=' . intval($this->userInfo['group_level']);
        $groupList = model('UserGroup')->loadData($whereGroup, 20);
        //模板赋值
        $this->assign('groupList', $groupList);
        $this->assign('userInfo', $this->userInfo);
        $this->show();
    }
    /**
     * 处理用户添加
     */
    public function addData()
    {
        if (empty($_POST['group_id'])) {
            $this->msg('用户组未选择！', false);
        }
        if (empty($_POST['username'])) {
            $this->msg('用户名不能为空！', false);
        }
        if (empty($_POST['nicename'])) {
            $this->msg('昵称不能为空！', false);
        }
        if (empty($_POST['email'])) {
            $this->msg('邮箱不能为空！', false);
        }
        if ($_POST['password'] <> $_POST['password2']) {
            $this->msg('两次密码输入不同！', false);
        }
        if ($_POST['level'] > $this->userInfo['level']) {
            $this->msg('用户等级不能高于' . $this->userInfo['level'], false);
        }
        //获取用户组信息
        $PostGroupInfo = model('UserGroup')->getInfo($_POST['group_id']);
        if ($PostGroupInfo['level'] > $this->userInfo['group_level']) {
            $this->msg('用户组等级不能高于' . $this->userInfo['group_level'], false);
        }
        if (model('User')->getUserEmail($_POST['email'])) {
            $this->msg('邮箱不能重复！', false);
        }
        if (model('User')->getUserInfo($_POST['username'])) {
            $this->msg('帐号不能重复！', false);
        }
        $_POST['password'] = md5($_POST['password']);
        model('User')->addData($_POST);
        $this->msg('用户添加成功！', 1);
    }
    /**
     * 编辑用户资料
     */
    public function edit()
    {
        $user_id = intval($_GET['user_id']);
        if (empty($user_id)) {
            $this->msg('无法获取用户ID！', false);
        }
        //用户信息
        $info = model('User')->getUser($user_id);
        //模板赋值
        $this->assign('info', $info);
        $this->show();
    }
    /**
     * 处理用户资料
     */
    public function editData()
    {
        $user_id = intval($_POST['user_id']);
        if (empty($user_id)) {
            $this->msg('无法获取用户ID！', false);
        }
        if (empty($_POST['nicename'])) {
            $this->msg('昵称不能为空！', false);
        }
        if (empty($_POST['email'])) {
            $this->msg('邮箱不能为空！', false);
        }
        if ($_POST['password'] <> $_POST['password2']) {
            $this->msg('两次密码输入不同！', false);
        }
        //获取用户组信息
        $data = array();
        $PostInfo = model('User')->getUser($user_id);
        if ($PostInfo['level'] >= $this->userInfo['level'] && $PostInfo['user_id'] <> $this->userInfo['user_id']) {
            $this->msg('您没有权限更改此用户！', false);
        }
        if (!empty($_POST['password'])) {
            if ($_POST['password'] <> $_POST['password2']) {
                $this->msg('两次密码输入不同！', false);
            }
            $data['password'] = md5($_POST['password']);
        }
        if (model('User')->getUserEmail($_POST['email'], $user_id)) {
            $this->msg('邮箱不能重复！', false);
        }
        $data['nicename'] = $_POST['nicename'];
        $data['email'] = $_POST['email'];
        $data['user_id'] = $user_id;
        model('User')->saveData($data);
        $this->msg('用户修改成功！', 1);
    }
    /**
     * 编辑用户信息
     */
    public function advanced()
    {
        $user_id = intval($_GET['user_id']);
        if (empty($user_id)) {
            $this->msg('无法获取用户ID！', false);
        }
        //管理组列表信息
        $whereGroup = 'level<=' . intval($this->userInfo['group_level']);
        $groupList = model('UserGroup')->loadData($whereGroup, 20);
        //用户信息
        $info = model('User')->getUser($user_id);
        //模板赋值
        $this->assign('groupList', $groupList);
        $this->assign('userInfo', $this->userInfo);
        $this->assign('info', $info);
        $this->show();
    }
    /**
     * 处理用户信息
     */
    public function advancedData()
    {
        $user_id = intval($_POST['user_id']);
        if (empty($user_id)) {
            $this->msg('无法获取用户ID！', false);
        }
        if (empty($_POST['group_id'])) {
            $this->msg('用户组未选择！', false);
        }
        if (empty($_POST['username'])) {
            $this->msg('用户名不能为空！', false);
        }
        if (empty($_POST['nicename'])) {
            $this->msg('昵称不能为空！', false);
        }
        if (empty($_POST['email'])) {
            $this->msg('邮箱不能为空！', false);
        }
        if ($_POST['level'] > $this->userInfo['level']) {
            $this->msg('用户等级不能高于' . $this->userInfo['level'], false);
        }
        //获取用户组信息
        $PostGroupInfo = model('UserGroup')->getInfo($_POST['group_id']);
        if ($PostGroupInfo['level'] > $this->userInfo['group_level']) {
            $this->msg('用户组等级不能高于' . $this->userInfo['group_level'], false);
        }
        //获取用户组信息
        $PostInfo = model('User')->getUser($user_id);
        if ($PostInfo['level'] >= $this->userInfo['level'] && $PostInfo['user_id'] <> $this->userInfo['user_id']) {
            $this->msg('您没有权限更改此用户！', false);
        }
        if (!empty($_POST['password'])) {
            if ($_POST['password'] <> $_POST['password2']) {
                $this->msg('两次密码输入不同！', false);
            }
            $_POST['password'] = md5($_POST['password']);
        } else {
            unset($_POST['password']);
        }
        if (model('User')->getUserEmail($_POST['email'], $user_id)) {
            $this->msg('邮箱不能重复！', false);
        }
        if (model('User')->getUserInfo($_POST['username'], $user_id)) {
            $this->msg('帐号不能重复！', false);
        }
        model('User')->saveData($_POST);
        $this->msg('用户修改成功！');
    }
    /**
     * 删除用户
     */
    public function del()
    {
        $user_id = intval($_POST['data']);
        if (empty($user_id)) {
            $this->msg('用户ID无法获取！', false);
        }
        if ($user_id == 1) {
            $this->msg('系统保留无法删除！', false);
        }
        //获取用户信息
        $PostInfo = model('User')->getUser($user_id);
        if ($PostInfo['level'] >= $this->userInfo['level'] && $PostInfo['user_id'] <> $this->userInfo['user_id']) {
            $this->msg('您没有权限删除此用户！', false);
        }
        model('User')->delData($user_id);
        $this->msg('用户删除成功！');
    }
}