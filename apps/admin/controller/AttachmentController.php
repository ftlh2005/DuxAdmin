<?php
/**
 * AttachmentController.php
 * 附件管理页面
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class AttachmentController extends AdminController
{
    /**
     * AJAX获取附件列表
     * 附件ID使用逗号分割
     * @param string $_POST['id'] 附件ID
     */
    public function fileList()
    {
        $id = in($_POST['id']);
        if (empty($id)) {
            $this->msg('附件获取错误！', false);
        }
        $list = model('Attachment')->getFileList($id);
        $idArray = explode(',', $id);
        if (empty($list)) {
            $this->msg('附件不存在！', false);
        }
        $listData = array();
        foreach ($list as $key => $value) {
            $listData[$value['file_id']] = $value;
        }
        $data = array();
        $i = 0;
        foreach ($idArray as $value) {
            $i++;
            $data[$i]['info'] = $listData[$value];
            $data[$i]['url'] = $listData[$value]['url'];
        }
        $this->msg($data);
    }
}