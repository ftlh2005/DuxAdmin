<?php
/**
 * MenuController.php
 * 系统菜单
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class MenuController extends AdminController
{
    /**
     * AJAX获取用户菜单
     */
    public function getMenu()
    {
        $data = model('Menu')->getMenuList();
        if (empty($data)) {
            $this->msg('没有发现菜单', 0);
        }
        //获取用户权限
        $userGroup = model('UserGroup')->getInfo($this->userInfo['group_id']);
        $menuPurview = unserialize($userGroup['menu_purview']);
        if (empty($menuPurview) || $this->userInfo['group_id'] == 1) {
            $this->msg($data);
        }
        $menuList = array();
        foreach ($data as $topLabel => $top) {
            $menuList[$topLabel]['name'] = $top['name'];
            if (!empty($top['menu'])) {
                foreach ($top['menu'] as $parentLabel => $parent) {
                    //二级菜单
                    if (!in_array($topLabel . '_' . $parentLabel, (array) $menuPurview)) {
                        continue;
                    }
                    $menuList[$topLabel]['menu'][$parentLabel]['name'] = $parent['name'];
                    foreach ($parent['menu'] as $key => $menu) {
                        //三级菜单
                        if (!in_array($menu['url'], (array) $menuPurview)) {
                            continue;
                        }
                        $menuList[$topLabel]['menu'][$parentLabel]['menu'][] = $menu;
                    }
                }
            }
        }
        $this->msg($menuList);
    }
}