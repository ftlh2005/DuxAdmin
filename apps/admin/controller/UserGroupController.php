<?php
/**
 * UserGroupController.php
 * 用户组管理页面
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class UserGroupController extends AdminController
{
    /**
     * 主框架页面
     */
    public function index()
    {
        //筛选条件
        $filterKeyword = urldecode($_GET['keyword']);
        $filterWhere = '';
        if (!empty($filterKeyword)) {
            $filterWhere .= ' AND name LIKE "%' . $filterKeyword . '%"';
        }
        //分页参数
        $listRows = 20;
        $urlArray = array(
            'page' => intval($_GET['page']),
            'keyword' => $filterKeyword
        );
        $url = url('UserGroup/index', $urlArray);
        $limit = $this->pageLimit($url, $listRows);
        //基础条件
        $where = 'level<=' . intval($this->userInfo['level']) . ' AND level<=' . intval($this->userInfo['group_level']) . $filterWhere;
        //用户组列表信息
        $list = model('UserGroup')->loadData($where, $limit);
        $count = model('UserGroup')->countData($where);
        //获取分页
        $page = $this->pageShow($count);
        //模板赋值
        $this->assign('list', $list);
        $this->assign('count', $count);
        $this->assign('page', $page);
        $this->show();
    }
    /**
     * 添加用户组
     */
    public function add()
    {
        //模板赋值
        $this->assign('action', 'add');
        $this->assign('actionName', '添加');
        $this->assign('userInfo', $this->userInfo);
        $this->show('usergroup/info');
    }
    /**
     * 处理用户组添加
     */
    public function addData()
    {
        if (empty($_POST['name'])) {
            $this->msg('用户组名不能为空！', false);
        }
        if ($_POST['level'] > $this->userInfo['group_level']) {
            $this->msg('用户组等级不能高于' . $this->userInfo['level'], false);
        }
        $data = array();
        $data['name'] = $_POST['name'];
        $data['level'] = intval($_POST['level']);
        $data['staus'] = intval($_POST['staus']);
        $data['admin'] = intval($_POST['admin']);
        model('UserGroup')->addData($_POST);
        $this->msg('用户组添加成功！', 1);
    }
    /**
     * 编辑用户组资料
     */
    public function edit()
    {
        $group_id = intval($_GET['group_id']);
        if (empty($group_id)) {
            $this->msg('无法获取用户组ID！', false);
        }
        //用户组信息
        $info = model('UserGroup')->getInfo($group_id);
        //模板赋值
        $this->assign('info', $info);
        $this->assign('action', 'edit');
        $this->assign('actionName', '修改');
        $this->assign('userInfo', $this->userInfo);
        $this->show('usergroup/info');
    }
    /**
     * 处理用户组资料
     */
    public function editData()
    {
        $group_id = intval($_POST['group_id']);
        if (empty($group_id)) {
            $this->msg('无法获取用户组ID！', false);
        }
        if (empty($_POST['name'])) {
            $this->msg('用户组名不能为空！', false);
        }
        if ($_POST['level'] > $this->userInfo['group_level']) {
            $this->msg('用户组等级不能高于' . $this->userInfo['level'], false);
        }
        //获取用户组信息
        $PostInfo = model('UserGroup')->getInfo($group_id);
        if ($PostInfo['level'] >= $this->userInfo['group_level'] && $PostInfo['group_id'] <> $this->userInfo['group_id']) {
            $this->msg('您没有权限更改此用户组！', false);
        }
        $data = array();
        $data['name'] = $_POST['name'];
        $data['level'] = intval($_POST['level']);
        $data['staus'] = intval($_POST['staus']);
        $data['admin'] = intval($_POST['admin']);
        $data['group_id'] = $group_id;
        model('UserGroup')->saveData($data);
        $this->msg('用户组修改成功！', 1);
    }
    /**
     * 编辑用户组权限
     */
    public function purview()
    {
        $group_id = intval($_GET['group_id']);
        if (empty($group_id)) {
            $this->msg('无法获取用户组ID！', false);
        }
        //用户组信息
        $info = model('UserGroup')->getInfo($group_id);
        if ($info['level'] >= $this->userInfo['group_level'] && $info['group_id'] <> $this->userInfo['group_id']) {
            $this->msg('您无法查看该用户权限！', false);
        }
        $AdminPurvewArray = unserialize($info['base_purview']);
        $AdminMenuArray = unserialize($info['menu_purview']);
        //模板赋值
        $this->assign('AdminPurvew', model('UserGroup')->getAdminPurview());
        $this->assign('AdminMenu', model('Menu')->getMenuList());
        $this->assign('AdminPurvewArray', $AdminPurvewArray);
        $this->assign('AdminMenuArray', $AdminMenuArray);
        $this->assign('info', $info);
        $this->show();
    }
    /**
     * 处理用户组信息
     */
    public function purviewData()
    {
        $group_id = intval($_POST['group_id']);
        if (empty($group_id)) {
            $this->msg('无法获取用户组ID！', false);
        }
        if ($_POST['level'] > $this->userInfo['group_level']) {
            $this->msg('用户组等级不足！', false);
        }
        //获取用户组信息
        $PostInfo = model('UserGroup')->getInfo($group_id);
        if ($PostInfo['level'] >= $this->userInfo['group_level'] && $PostInfo['group_id'] <> $this->userInfo['group_id']) {
            $this->msg('您没有权限更改此用户组！', false);
        }
        $data = array();
        $data['group_id'] = $group_id;
        $data['base_purview'] = serialize($_POST['AdminPurvew']);
        $data['menu_purview'] = serialize($_POST['AdminMenu']);
        model('UserGroup')->saveData($data);
        $this->msg('用户组修改成功！');
    }
    /**
     * 删除用户组
     */
    public function del()
    {
        $group_id = intval($_POST['data']);
        if (empty($group_id)) {
            $this->msg('用户组ID无法获取！', false);
        }
        if ($group_id == 1) {
            $this->msg('系统保留无法删除！', false);
        }
        //获取用户组信息
        $PostInfo = model('UserGroup')->getInfo($group_id);
        if ($PostInfo['level'] >= $this->userInfo['group_level'] && $PostInfo['group_id'] <> $this->userInfo['group_id']) {
            $this->msg('您没有权限删除此用户组！', false);
        }
        model('UserGroup')->delData($group_id);
        $this->msg('用户组删除成功！');
    }
}