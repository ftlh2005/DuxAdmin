<?php
/**
 * UploadController.php
 * 通用上传页面
 * @author Life <349865361@qq.com>
 * @version 20140109
 */
class UploadController extends AdminController
{
    /**
     * AJAX上传请求地址
     */
    public function index()
    {
        $info = model('Upload')->uploadData($_FILES);
        header('Content-type: text/html; charset=UTF-8');
        if (is_array($info)) {
            echo json_encode(array(
                'error' => 0,
                'url' => $info['url'],
                'info' => $info
            ));
        } else {
            echo json_encode(array(
                'error' => 1,
                'message' => $info
            ));
        }
    }
}