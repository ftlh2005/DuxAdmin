<?php
/**
 * IndexController.php
 * 系统主页面
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class IndexController extends AdminController
{
    /**
     * 主框架页面
     */
    public function index()
    {
        $this->assign('userInfo', $this->userInfo);
        $this->assign('adminInfo', config('ADMIN'));
        $this->display();
    }
    /**
     * 欢迎页面
     */
    public function home()
    {
        $this->assign('verInfo', config('COPYRIGHT'));
        $this->show();
    }
}