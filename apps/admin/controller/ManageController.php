<?php
/**
 * ManageController.php
 * 系统管理功能
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class ManageController extends AdminController
{
    /**
     * 缓存管理
     */
    public function cache()
    {
        $this->rootPath = realpath(ROOT_PATH);
        $this->show();
    }
    /**
     * AJAX获取缓存信息
     * @param int $_POST['type'] 缓存类型
     */
    public function getCacheInfo()
    {
        $type = intval($_POST['type']);
        if (empty($type)) {
            $this->msg('缓存类型无法获取！', false);
        }
        $file = model('Cache')->getFile($type);
        if (is_dir($file) && file_exists($file)) {
            $name = '目录';
            $size = dir_size($file);
        } else if (is_file($file) && file_exists($file)) {
            $name = '文件';
            $size = filesize($file);
        } else {
            $this->msg('<font color="#009900">文件不存在或未开启缓存</font>');
        }
        $size = round($size / pow(1024, 1), 2) . "KB";
        $this->msg('<font color="#009900">' . $name . '大小为' . $size . '</font>');
    }
    /**
     * 情况系统缓存
     * @param int $_POST['type'] 缓存类型
     */
    public function clearCache()
    {
        $type = intval($_POST['type']);
        if (empty($type)) {
            $this->msg('缓存类型无法获取！', false);
        }
        $file = model('Cache')->getFile($type);
        if (is_dir($file) && file_exists($file)) {
            $status = @del_dir($file);
        } else if (is_file($file) && file_exists($file)) {
            $status = @unlink($file);
        } else {
            $this->msg('<font color="#009900">缓存不存在！</font>');
        }
        if ($status) {
            $this->msg('<font color="#009900">清除缓存完毕！</font>');
        } else {
            $this->msg('<font color="#009900">清除缓存失败！</font>');
        }
    }
}