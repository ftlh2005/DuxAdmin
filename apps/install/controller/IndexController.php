<?php
/**
 * IndexController.php
 * 安装页面
 * @author Life <349865361@qq.com>
 * @version 20140110
 */
class IndexController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->lockFile = ROOT_PATH . 'cache/install.lock';
        if (file_exists($this->lockFile)) {
            $this->redirect(__ROOT__ . '/');
        }
    }
    /**
     * 安装页面
     */
    public function index()
    {
        if (PHP_VERSION < '5.2.0') {
            $error = 'PHP版本低于5.2，无法进行安装！';
        }
        if (!function_exists("session_start")) {
            $error = '系统不支持session，无法进行安装！';
        }
        $this->assign('adminInfo', config('ADMIN'));
        $this->assign('error', $error);
        $this->display();
    }
    /**
     * 安装处理
     */
    public function postData()
    {
        $data = in($_POST);
        $configDb = $data['DB'];
        $configSU = $data['SU'];
	 	$configSU['NAME'] = isset($configSU['NAME']) ? trim($configSU['NAME']) : '';
	    $configSU['USER'] = isset($configSU['USER']) ? trim($configSU['USER']) : '';
	    $configSU['PWD'] = isset($configSU['PWD']) ? trim($configSU['PWD']) : '';
	    $configSU['EMAIL'] = isset($configSU['EMAIL']) ? trim($configSU['EMAIL']) : '';
		if($configSU['NAME'] == '' || $configSU['USER'] == ''|| $configSU['PWD'] == ''|| $configSU['EMAIL'] == ''){
			$this->msg('您填写的信息不完整，请核对', false);
		}
		if (!preg_match("/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/",$configSU['EMAIL'])){
			$this->msg('电子邮箱格式错误', false);
		}
	    $configSU['PWD'] = md5($configSU['PWD']);
        $link = @mysql_connect($configDb['DB_HOST'] . ':' . $configDb['DB_PORT'], $configDb['DB_USER'], $configDb['DB_PWD']);
        if (!$link) {
            $this->msg('数据库连接失败，请检查连接信息是否正确！', false);
        }
        $mysqlInfo = mysql_get_server_info($link);
        if ($mysqlInfo < '5.1.0') {
            $this->msg('mysql版本低于5.1，无法继续安装！', false);
        }
        $status = @mysql_select_db($configDb['DB_NAME'], $link);
        if (!$status) {
	  		if(!model('Install')->create_db($configDb)){
	            $this->msg('数据库'. $configDb['DB_NAME'].'自动创建失败，请手动建立数据库！' , false);
	            exit;
	        }
        }
        $dbPrefix = $configDb['DB_PREFIX'];
        if (empty($dbPrefix)) {
            $dbPrefix = 'dux_';
        }
        $dbData = ROOT_PATH . 'apps/' . APP_NAME . '/data/data.sql';
        $sqlData = Install::mysql($dbData, 'dux_', $dbPrefix);
        if (!model('Install')->runSql($configDb, $sqlData)) {
            $this->msg('数据导入失败，请检查后手动删除数据库重新安装！', false);
        }
        $dbData = "UPDATE `{$dbPrefix}user` SET username = '{$configSU['USER']}',password='{$configSU['PWD']}',nicename='{$configSU['NAME']}',email='{$configSU['EMAIL']}' WHERE user_id = 1";
        $sqlData=model('Install')->sql_db($configDb, $dbData);
        unset($data['SU']);
        if (!api('admin', 'setConfig', $data)) {
            $this->msg('配置文件写入失败，请确保ini目录已经文件有写入权限！', fasle);
        }
        @fopen($this->lockFile, 'w');
        $this->msg('安装成功！', fasle);
    }
    /**
     * 安装成功
     */
    public function success()
    {
        $this->display();
    }
}