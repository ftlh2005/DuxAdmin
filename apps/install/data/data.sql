-- phpMyAdmin SQL Dump
-- version 4.0.2
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2014 年 01 月 13 日 11:17
-- 服务器版本: 5.6.13
-- PHP 版本: 5.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

-- --------------------------------------------------------

--
-- 表的结构 `dux_attachment`
--

DROP TABLE IF EXISTS `dux_attachment`;
CREATE TABLE IF NOT EXISTS `dux_attachment` (
  `file_id` int(10) NOT NULL AUTO_INCREMENT,
  `relation_id` int(10) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `original` varchar(250) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `ext` varchar(250) DEFAULT NULL,
  `size` int(10) DEFAULT NULL,
  `time` int(10) DEFAULT NULL,
  `app` varchar(250) DEFAULT NULL,
  `relation_key` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `relation_id` (`relation_id`),
  KEY `relation_key` (`relation_key`),
  KEY `ext` (`ext`),
  KEY `time` (`time`) USING BTREE,
  KEY `app` (`app`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `dux_user`
--

DROP TABLE IF EXISTS `dux_user`;
CREATE TABLE IF NOT EXISTS `dux_user` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '用户IP',
  `group_id` int(10) NOT NULL DEFAULT '1' COMMENT '用户组ID',
  `username` varchar(20) NOT NULL COMMENT '登录名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `nicename` varchar(20) DEFAULT NULL COMMENT '昵称',
  `email` varchar(50) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '状态',
  `level` int(5) DEFAULT '1' COMMENT '等级',
  `reg_time` int(10) DEFAULT NULL COMMENT '注册时间',
  `login_num` int(10) DEFAULT '1' COMMENT '登录次数',
  `last_login_time` int(10) DEFAULT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(15) DEFAULT '未知' COMMENT '登录IP',
  PRIMARY KEY (`user_id`),
  KEY `username` (`username`),
  KEY `group_id` (`group_id`) USING BTREE,
  KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='管理员信息表' AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `dux_user`
--

INSERT INTO `dux_user` (`user_id`, `group_id`, `username`, `password`, `nicename`, `email`, `status`, `level`, `reg_time`, `login_num`, `last_login_time`, `last_login_ip`) VALUES
(1, 1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'duxcms', 'admin@duxcms.com', 1, 250, 1350138971, 1, 1389579048, '127.0.0.1');

-- --------------------------------------------------------

--
-- 表的结构 `dux_user_group`
--

DROP TABLE IF EXISTS `dux_user_group`;
CREATE TABLE IF NOT EXISTS `dux_user_group` (
  `group_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `base_purview` text,
  `menu_purview` text,
  `level` int(10) DEFAULT '3',
  `status` tinyint(1) DEFAULT '1',
  `admin` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`group_id`),
  KEY `name` (`name`),
  KEY `level` (`level`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `dux_user_group`
--

INSERT INTO `dux_user_group` (`group_id`, `name`, `base_purview`, `menu_purview`, `level`, `status`, `admin`) VALUES
(1, '超级管理组', 'a:7:{i:0;s:13:"admin_Setting";i:1;s:19:"admin_Setting_admin";i:2;s:25:"admin_Setting_performance";i:3;s:20:"admin_Setting_upload";i:4;s:12:"admin_Manage";i:5;s:18:"admin_Manage_cache";i:6;s:21:"admin_AppManage_index";}', 'a:30:{i:0;s:13:"index_default";i:1;s:29:"/index.php?r=admin/Index/home";i:2;s:14:"system_setting";i:3;s:32:"/index.php?r=admin/Setting/admin";i:4;s:38:"/index.php?r=admin/Setting/performance";i:5;s:33:"/index.php?r=admin/Setting/upload";i:6;s:13:"system_manage";i:7;s:31:"/index.php?r=admin/Manage/cache";i:8;s:34:"/index.php?r=admin/AppManage/index";i:9;s:11:"system_user";i:10;s:29:"/index.php?r=admin/User/index";i:11;s:34:"/index.php?r=admin/UserGroup/index";i:12;s:12:"help_example";i:13;s:29:"/index.php?r=help/Index/table";i:14;s:28:"/index.php?r=help/Index/form";i:15;s:12:"help_element";i:16;s:32:"/index.php?r=help/Element/jscode";i:17;s:30:"/index.php?r=help/Element/icon";i:18;s:37:"/index.php?r=help/Element/typesetting";i:19;s:30:"/index.php?r=help/Element/form";i:20;s:31:"/index.php?r=help/Element/table";i:21;s:12:"help_default";i:22;s:33:"/index.php?r=help/Docs/competence";i:23;s:33:"/index.php?r=help/Docs/competence";i:24;s:27:"/index.php?r=help/Docs/menu";i:25;s:34:"/index.php?r=help/Docs/explanation";i:26;s:27:"/index.php?r=help/Docs/base";i:27;s:32:"/index.php?r=help/Docs/directory";i:28;s:10:"help_about";i:29;s:29:"/index.php?r=help/About/index";}', 250, 1, 1);

-- --------------------------------------------------------

--
-- 表的结构 `dux_user_log`
--

DROP TABLE IF EXISTS `dux_user_log`;
CREATE TABLE IF NOT EXISTS `dux_user_log` (
  `log_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `time` int(10) DEFAULT NULL,
  `ip` varchar(250) DEFAULT NULL,
  `type` int(10) DEFAULT '1',
  `content` text,
  PRIMARY KEY (`log_id`),
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
