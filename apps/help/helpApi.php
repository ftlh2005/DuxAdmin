<?php
class helpApi extends BaseApi
{
    //菜单Api
    public function apiAdminMenu()
    {
        return array(
            'help' => array(
                'name' => '帮助',
                'order' => 10,
                'menu' => array(
                    'example' => array(
                        'name' => '页面示例',
                        'menu' => array(
                            array(
                                'name' => '表格示例',
                                'ico' => '&#xf00a;',
                                'url' => url('Index/table'),
                                'order' => 0
                            ),
                            array(
                                'name' => '表单示例',
                                'ico' => '&#xf0f7;',
                                'url' => url('Index/form'),
                                'order' => 1
                            )
                        )
                    ),
                    'element' => array(
                        'name' => '前端部分',
                        'menu' => array(
                            array(
                                'name' => 'js说明',
                                'ico' => '&#xf022;',
                                'url' => url('Element/jscode'),
                                'order' => 1
                            ),
                            array(
                                'name' => '图标',
                                'ico' => '&#xf03e;',
                                'url' => url('Element/icon'),
                                'order' => 2
                            ),
                            array(
                                'name' => '排版',
                                'ico' => '&#xf039;',
                                'url' => url('Element/typesetting'),
                                'order' => 3
                            ),
                            array(
                                'name' => '表单元素',
                                'ico' => '&#xf022;',
                                'url' => url('Element/form'),
                                'order' => 4
                            ),
                            array(
                                'name' => '表格元素',
                                'ico' => '&#xf022;',
                                'url' => url('Element/table'),
                                'order' => 5
                            )
                        )
                    ),
                    'default' => array(
                        'name' => '后端部分',
                        'menu' => array(
                            array(
                                'name' => '目录结构',
                                'url' => url('Docs/directory'),
                                'order' => 0
                            ),
                            array(
                                'name' => '构造APP',
                                'url' => url('Docs/docApp'),
                                'order' => 1
                            ),
                            array(
                                'name' => '系统API',
                                'url' => url('Docs/systemApi'),
                                'order' => 2
                            ),
                            array(
                                'name' => '模板标签',
                                'url' => url('Docs/docTemplate'),
                                'order' => 5
                            )
                        )
                    ),
                    'about' => array(
                        'name' => '关于',
                        'menu' => array(
                            array(
                                'name' => '介绍',
                                'url' => url('Index/about'),
                                'order' => 0
                            )
                        )
                    )
                )
            )
        );
    }
}