<?php
class IndexController extends AdminController
{
    public function table()
    {
        $this->show();
    }
    public function form()
    {
        $this->categoryList = model('help')->testList(0);
        $this->show();
    }
    public function menuList()
    {
        $pid = intval($_POST['pid']);
        //演示数据
        $list = model('help')->testList($pid);
        $this->msg($list, true);
    }
    public function addForm()
    {
        $this->msg('添加成功！');
    }
    public function editForm()
    {
        $this->msg('修改成功！');
    }
    public function delForm()
    {
        $this->msg('删除成功！');
    }
    public function about()
    {
        $this->show();
    }
}