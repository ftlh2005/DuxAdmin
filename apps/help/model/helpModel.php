<?php
class HelpModel extends BaseModel
{
    public function testList($pid = 0)
    {
        $data = array();
        $data[] = array(
            'id' => 1,
            'pid' => 0,
            'name' => '中国'
        );
        $data[] = array(
            'id' => 2,
            'pid' => 0,
            'name' => '美国'
        );
        $data[] = array(
            'id' => 3,
            'pid' => 0,
            'name' => '韩国'
        );
        $data[] = array(
            'id' => 4,
            'pid' => 1,
            'name' => '北京'
        );
        $data[] = array(
            'id' => 5,
            'pid' => 1,
            'name' => '上海'
        );
        $data[] = array(
            'id' => 6,
            'pid' => 1,
            'name' => '广西'
        );
        $data[] = array(
            'id' => 7,
            'pid' => 6,
            'name' => '桂林'
        );
        $data[] = array(
            'id' => 8,
            'pid' => 6,
            'name' => '南宁'
        );
        $data[] = array(
            'id' => 9,
            'pid' => 6,
            'name' => '柳州'
        );
        $data[] = array(
            'id' => 10,
            'pid' => 2,
            'name' => '纽约'
        );
        $data[] = array(
            'id' => 11,
            'pid' => 2,
            'name' => '华盛顿'
        );
        $data[] = array(
            'id' => 12,
            'pid' => 3,
            'name' => '首尔'
        );
        $data[] = array(
            'id' => 13,
            'pid' => 4,
            'name' => '北京市'
        );
        //处理数据
        $list = array();
        foreach ($data as $key => $value) {
            if ($value['pid'] == $pid) {
                $list[] = $value;
            }
        }
        return $list;
    }
}