<?php
/**
 * IndexController.php
 * 默认页面
 * @author Life <349865361@qq.com>
 * @version 20140113
 */
class IndexController extends BaseController
{
    /**
     * 首页
     */
    public function index()
    {
        @header("Content-type: text/html; charset=utf-8");
        echo '这是默认页面模块，您可以进行更换<a href="' . url('admin/Index/index') . '">进入后台管理</a>';
    }
}