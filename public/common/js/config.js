//核心配置
Do.setConfig('coreLib', [baseDir + 'jquery.js']);
//base
Do.add('base',
{
    path : baseDir + 'base/base.js'
}
);

//dialog
Do.add('dialogCss',
{
    path : baseDir + 'dialog/skins/default.css',
    type : 'css'
}
);
Do.add('dialogJs',
{
    path : baseDir + 'dialog/artDialog.js',
    requires : ['dialogCss']
}
);
Do.add('dialog',
{
    path : baseDir + 'dialog/iframeTools.js',
    requires : ['dialogJs']
}
);

//calendar
Do.add('calendar',
{
    path : baseDir + 'calendar/lhgcalendar.js'
}
);

//color
Do.add('colorCss',
{
    path : baseDir + 'color/style.css',
    type : 'css'
}
);
Do.add('color',
{
    path : baseDir + 'color/soColorPacker.js',
    requires : ['colorCss']
}
);

//editor
Do.add('editorSrc',
{
    path : baseDir + 'editor/kindeditor-min.js'
}
);
Do.add('editor',
{
    path : baseDir + 'editor/lang/zh_CN.js',
    requires : ['editorSrc']
}
);

//form
Do.add('formCss',
{
    path : baseDir + 'form/style.css',
    type : 'css'
}
);
Do.add('formJs',
{
    path : baseDir + 'form/validform.js'
}
);
Do.add('form',
{
    path : baseDir + 'form/Validform_Datatype.js',
    requires : ['formCss', 'formJs']
}
);

//scrollbar
Do.add('scrollbarJs',
{
    path : baseDir + 'scrollbar/jquery.mousewheel.js'
}
);
Do.add('scrollbar',
{
    path : baseDir + 'scrollbar/jquery.jscrollpane.min.js',
    requires : ['scrollbarJs']
}
);

//tabs
Do.add('tabs',
{
    path : baseDir + 'tabs/jquery-powerSwitch-min.js'
}
);

//tip
Do.add('tipCss',
{
    path : baseDir + 'tip/style.css',
    type : 'css'
}
);
Do.add('tip',
{
    path : baseDir + 'tip/powerFloat.js',
    requires : ['tipCss']
}
);

//sort
Do.add('sort',
{
    path : baseDir + 'upload/jquery.sortable.js'
}
);

//Upload
Do.add('uploadSrc',
{
    path : baseDir + 'upload/plupload.full.min.js'
}
);
Do.add('upload',
{
    path : baseDir + 'upload/lang/zh_CN.js',
    requires : ['uploadSrc']
}
);

//prettify
Do.add('prettifyCss',
{
    path : baseDir + 'code/prettify.css',
    type : 'css'
}
);
Do.add('prettify',
{
    path : baseDir + 'code/prettify.js',
    requires : ['prettifyCss']
}
);

//调试函数
function debug(obj)
{
    if (typeof console != 'undefined')
    {
        console.log(obj);
    }
}
debug('欢迎使用DuxAmin管理系统，如果您有反馈请发送邮件到349865361@qq.com');
