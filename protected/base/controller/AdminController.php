<?php
class AdminController extends BaseController
{
    protected $appID = 'admin';
    public function __construct()
    {
        $appID = config('appID');
        $this->appID = empty($appID) ? $this->appID : $appID;
        $this->appID = $this->appConfig['COOKIE_PREFIX'] . $this->appID;
        @session_start();
        $this->checkLogin();
        parent::__construct();
    }
    //模板显示含公共
    protected function show($tpl = '', $app = '')
    {
        $content = $this->display($tpl, true, true, $app);
        $layout = $this->display('common', true, true, 'admin');
        echo str_replace('<!--common-->', $content, $layout);
    }
    //权限检测
    protected function checkPurview()
    {
        if ($this->userInfo['user_id'] == 1 || $this->userInfo['group_id'] == 1) {
            return true;
        }
        $userGroup = api('admin', 'getGroupInfo', $this->userInfo['group_id']);
        $basePurview = unserialize($userGroup['base_purview']);
        $purviewInfo = api(APP_NAME, 'apiAdminPurview');
        if (empty($purviewInfo)) {
            return true;
        }
        $controller = $purviewInfo[CONTROLLER_NAME];
        if (empty($controller['auth'])) {
            return true;
        }
        $action = $controller['auth'][ACTION_NAME];
        if (empty($action)) {
            return true;
        }
        $current = APP_NAME . '_' . CONTROLLER_NAME;
        if (!in_array($current, (array) $basePurview)) {
            $this->msg('您没有权限访问此功能！', 0);
        }
        $current = APP_NAME . '_' . CONTROLLER_NAME . '_' . ACTION_NAME;
        if (!in_array($current, (array) $basePurview)) {
            $this->msg('您没有权限访问此功能！', 0);
        }
        return true;
    }
    //登录检测
    protected function checkLogin()
    {
        //不需要登录验证的页面数组
        $noLogin = array(
            'admin' => array(
                'Login' => array(
                    'index',
                    'postLogin'
                )
            )
        );
        //如果当前访问是无需登录验证，则直接返回
        if (isset($noLogin[APP_NAME]) && isset($noLogin[APP_NAME][CONTROLLER_NAME]) && in_array(ACTION_NAME, $noLogin[APP_NAME][CONTROLLER_NAME])) {
            return true;
        }
        //没有登录,则跳转到登录页面
        if (!$this->isLogin()) {
            $this->redirect(url('admin/Login/index'));
        }
        $this->checkPurview();
        return true;
    }
    //获取登录
    protected function getLogin()
    {
        return $_SESSION[$this->appID . '_uid'];
    }
    //判断是否登录
    protected function isLogin()
    {
        $appconfig = config('APP');
        if ($_REQUEST['SAFE_KEY'] && $_REQUEST['SAFE_KEY'] == $appconfig['SAFE_KEY']) {
            return true;
        }
        if (empty($_SESSION[$this->appID . '_uid'])) {
            return false;
        } else {
            $this->userInfo = $_SESSION[$this->appID . '_uid'];
            if (!$this->userInfo['admin']) {
                return false;
            }
            return true;
        }
    }
    //设置登录
    protected function setLogin($userInfo)
    {
        $_SESSION[$this->appID . '_uid'] = $userInfo;
    }
    //退出登录
    protected function clearLogin($url = '')
    {
        $_SESSION[$this->appID . '_uid'] = NULL;
        if (!empty($url)) {
            $this->redirect($url);
        }
        return true;
    }
}