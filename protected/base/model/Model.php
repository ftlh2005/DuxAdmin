<?php
class Model
{
    public $model = NULL;
    protected $db = NULL;
    protected $table = "";
    protected $ignoreTablePrefix = false;
    public function __construct($database = 'DB', $force = false)
    {
        $this->model = self::connect(config($database), $force);
        $this->db = $this->model->db;
    }
    static public function connect($config, $force = false)
    {
        static $model = NULL;
        if ($force == true || empty($model)) {
            $model = new cpModel($config);
        }
        return $model;
    }
    public function query($sql)
    {
        return $this->model->query($sql);
    }
    public function find($condition = '', $field = '', $order = '')
    {
        return $this->model->table($this->table, '', $this->ignoreTablePrefix)->field($field)->where($condition)->order($order)->find();
    }
    public function select($condition = '', $field = '', $order = '', $limit = '')
    {
        return $this->model->table($this->table, '', $this->ignoreTablePrefix)->field($field)->where($condition)->order($order)->limit($limit)->select();
    }
    public function count($condition = '')
    {
        return $this->model->table($this->table, '', $this->ignoreTablePrefix)->where($condition)->count();
    }
    public function insert($data = array())
    {
        $data = $this->format_data_by_fill($data);
        $id = $this->model->table($this->table, '', $this->ignoreTablePrefix)->data($data)->insert();
        $this->userLog('add');
        return $id;
    }
    public function update($condition, $data = array())
    {
        $data = $this->format_data_by_fill($data);
        $status = $this->model->table($this->table, '', $this->ignoreTablePrefix)->data($data)->where($condition)->update();
        $this->userLog('edit');
        return $status;
    }
    public function delete($condition)
    {
        $status = $this->model->table($this->table, '', $this->ignoreTablePrefix)->where($condition)->delete();
        $this->userLog('del');
        return $status;
    }
    public function getSql()
    {
        return $this->model->getSql();
    }
    public function escape($value)
    {
        return $this->model->escape($value);
    }
    public function cache($time = 0)
    {
        $this->model->cache($time);
        return $this;
    }
    public function format_data_by_fill(array $data = array())
    {
        $defaultData = $this->fields_default();
        $array = array();
        if (empty($data))
            return $array;
        foreach ($defaultData as $key => $value) {
            if (isset($data[$key])) {
                $array[$key] = $data[$key];
            }
        }
        return $this->quote_array($array);
    }
    public function fields_default()
    {
        $data = $this->model->table($this->table)->getFields();
        foreach ($data as $field) {
            $fields_default[$field['Field']] = $field['Default'];
        }
        return $fields_default;
    }
    public function quote_array(&$valueArr)
    {
        return array_map(array(
            &$this,
            'quote'
        ), $valueArr);
    }
    public function quote($value)
    {
        if (is_null($value))
            return 'NULL';
        if (is_bool($value))
            return $value ? 1 : 0;
        if (is_int($value))
            return (int) $value;
        if (is_float($value))
            return (float) $value;
        if (@get_magic_quotes_gpc())
            $value = @stripslashes($value);
        return $value;
    }
    public function userLog($action)
    {
        switch ($action) {
            case 'add':
                $type = 2;
                break;
            case 'edit':
                $type = 3;
                break;
            case 'del':
                $type = 4;
                break;
        }
        if (!empty($type)) {
            $data = array();
            $data['user_id'] = $_SESSION['admin_uid']['user_id'];
            $data['time'] = time();
            $data['type'] = $type;
            $data['ip'] = get_client_ip();
            $data['content'] = APP_NAME . ':' . CONTROLLER_NAME . ':' . ACTION_NAME . ':' . $this->table;
            api('admin', 'setUserLog', $data);
            return;
        }
    }
}