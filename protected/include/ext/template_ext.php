<?php
if( !function_exists('tpl_parse_ext')) {
	function tpl_parse_ext($template){
		return template_ext($template);
	}
}
//模板扩展函数
function template_ext($template){

    //替换文件路径
    $template = preg_replace("/<(.*?)(src=|href=|value=|background=)[\"|\'](images\/|img\/|css\/|js\/|style\/)(.*?)[\"|\'](.*?)>/i",
        "<$1$2\"".__ROOT__."/<?php echo Config('TPL_TEMPLATE_PATH').'/'.Config('TPL_TEMPLATE_NAME'); ?>/"."$3$4\"$5>", $template);

	
	//php标签
	/*
	 {php echo phpinfo();}	=>	<?php echo phpinfo(); ?>
	*/
	$template = preg_replace ( "/\{php\s+(.+)\}/", "<?php \\1?>", $template );
		
	//if 标签
	/**
	 * {if $name==1}		=>	<?php if ($name==1){ ?>
	 * {elseif $name==2}	=>	<?php } elseif ($name==2){ ?>
	 * {else}				=>	<?php } else { ?>
	 * {/if}				=>	<?php } ?>
	 */
	$template = preg_replace ( "/\{if\s+(.+?)\}/", "<?php if(\\1) { ?>", $template );
	$template = preg_replace ( "/\{else\}/", "<?php } else { ?>", $template );
	$template = preg_replace ( "/\{elseif\s+(.+?)\}/", "<?php } elseif (\\1) { ?>", $template );
	$template = preg_replace ( "/\{\/if\}/", "<?php } ?>", $template );
		
	//for 标签
	/**
	 * {for $i=0;$i<10;$i++}	=>	<?php for($i=0;$i<10;$i++) { ?>
	 * {/for}					=>	<?php } ?>
	 */
	$template = preg_replace("/\{for\s+(.+?)\}/","<?php for(\\1) { ?>",$template);
	$template = preg_replace("/\{\/for\}/","<?php } ?>",$template);
		
	//loop 标签
	/*
	 * {loop $arr $vo}			=>	<?php $n=1; if (is_array($arr) foreach($arr as $vo){ ?>
	 * {loop $arr $key $vo}	=>	<?php $n=1; if (is_array($array) foreach($arr as $key => $vo){ ?>
	 * {/loop}					=>	<?php $n++;}unset($n) ?>
	 */
	$template = preg_replace ( "/\{loop\s+(\S+)\s+(\S+)\}/", "<?php \$n=1;if(is_array(\\1)) foreach(\\1 AS \\2) { ?>", $template );
	$template = preg_replace ( "/\{loop\s+(\S+)\s+(\S+)\s+(\S+)\}/", "<?php \$n=1; if(is_array(\\1)) foreach(\\1 AS \\2 => \\3) { ?>", $template );
	$template = preg_replace ( "/\{\/loop\}/", "<?php \$n++;}unset(\$n); ?>", $template );
		
	//函数 标签
	/**
	 * {date('Y-m-d H:i:s')}	=>	<?php echo date('Y-m-d H:i:s');?> 
	 * {$date('Y-m-d H:i:s')}	=>	<?php echo $date('Y-m-d H:i:s');?> 
	 */
	$template = preg_replace ( "/\{([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff:]*\(([^{}]*)\))\}/", "<?php echo \\1;?>", $template );
	$template = preg_replace ( "/\{(\\$[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff:]*\(([^{}]*)\))\}/", "<?php echo \\1;?>", $template );

    //HTML输出
    $template = preg_replace ( "/\{(\\$[a-z0-9_]+)\.([a-z0-9_]+)\s+html\}/iU", "<?php echo html_out($1['$2']); ?>", $template);
    $template = preg_replace ( "/\{(\\$[a-z0-9_]+)\.([a-z0-9_]+)\.([a-z0-9_]+)\s+html\}/iU", "<?php echo html_out($1[\'$2\'][\'$3\']); ?>", $template);

    //时间标签
    $template = preg_replace ( "/\{(\\$[a-z0-9_]+)\.([a-z0-9_]+)\s+time\=[\"|\'](.*)[\"|\']\}/iU", "<?php echo date('$3',$1['$2']); ?>", $template);
    $template = preg_replace ( "/\{(\\$[a-z0-9_]+)\.([a-z0-9_]+)\.([a-z0-9_]+)\s+time\=[\"|\'](.*)[\"|\']\}/iU", "<?php echo date('$4',$1[\'$2\'][\'$3\']); ?>", $template);

    //为空默认值
    $template = preg_replace ( "/\{(\\$[a-z0-9_]+)\.([a-z0-9_]+)\s+empty\=[\"|\'](.*)[\"|\']\}/iU", "<?php if(!empty($1['$2'])){echo $1['$2'];}else{echo '$3';}?>", $template);
    $template = preg_replace ( "/\{(\\$[a-z0-9_]+)\.([a-z0-9_]+)\.([a-z0-9_]+)\s+empty\=\"(.*)\"\}/iU", "<?php if(!empty($1[\'$2\'][\'$3\'])){echo $1[\'$2\'][\'$3\'];}else{echo '$4';}?>", $template);

    //不存在默认值
    $template = preg_replace ( "/\{(\\$[a-z0-9_]+)\.([a-z0-9_]+)\s+isset\=[\"|\'](.*)[\"|\']\}/iU", "<?php if(isset($1['$2'])){echo $1['$2'];}else{echo '$3';}?>", $template);
    $template = preg_replace ( "/\{(\\$[a-z0-9_]+)\.([a-z0-9_]+)\.([a-z0-9_]+)\s+isset\=\"(.*)\"\}/iU", "<?php if(isset($1[\'$2\'][\'$3\'])){echo $1[\'$2\'][\'$3\'];}else{echo '$4';}?>", $template);

	//数组标签
	/*{$info.xxx}替换成 <?php echo $info['xxx'] ?>*/
	$template = preg_replace ( "/\{(\\$[a-z0-9_]+)\.([a-z0-9_]+)\}/i", "<?php echo $1['$2']; ?>", $template);
    $template = preg_replace ( "/\{(\\$[a-z0-9_]+)\.([a-z0-9_]+)\.([a-z0-9_]+)\}/i", "<?php echo $1['$2']['$3']; ?>", $template);


	return $template;

}