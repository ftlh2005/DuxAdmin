<?php
class cpSessionCache {
	public $config =array(); //配置
	public $cache = NULL;	//缓存对象
	public function __construct( $config = array() ) {
		$this->config = array_merge(cpConfig::get('SESSION'), $config);	//参数配置
    }
    public function run() {
    	if(!$this->config['DB_CACHE_ON']){
    		@session_start();
    		return;
    	}
    	if (!is_object($this->cache)) {
			require_once( dirname(__FILE__) . '/cpCache.class.php' );
			$this->cache = new cpCache($this->config, $this->config['DB_CACHE_TYPE']);
		}
    	ini_set ( 'session.save_handler','user');
    	session_set_save_handler ( array ($this, 'open' ), array ($this, 'close' ), array ($this, 'read' ), array ($this, 'write' ), array ($this, 'destroy' ), array ($this, 'gc' ) );
    	@session_start();
    }
    /**
	 * 打开
	 */
	public function open() {
		return true;
	}
	/**
	 * 关闭
	 */
	public function close() {
		return true;
	}
	/**
	 * 读取
	 */
	public function read($id) {

		$where['id']=$id;
		return $this->cache->get($id);
	}
	/**
	 * 写入
	 */
	public function write($id, $datas) {
		return $this->cache->set($id, $datas, -1);
	}
	/**
	 * 摧毁
	 */
	public function destroy($id) {
		return $this->cache->del($id);
	}
	/**
	 * 回收
	 */
	public function gc($max) {
		return true;
	}

    
	
	




}